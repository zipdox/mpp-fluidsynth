let timeOuts = [];

function addTimeout(a){
    let timeout;
    timeout = setTimeout(function() {
        postMessage({
            args: a.data.args
        });
        timeOuts = timeOuts.filter(t=>t!=timeout);
    }, a.data.delay);
    timeOuts.push(timeout);
}

self.onmessage = function(a) {
    if(a.data == null){
        for(let timeout of timeOuts){
            clearTimeout(timeout);
        }
        timeOuts = [];
    }else{
        addTimeout(a);
    }
};
